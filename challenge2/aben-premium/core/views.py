from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView

from subscription.models import Plan


class HomeView(TemplateView):
    template_name = "core/index.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        queryset = Plan.objects.filter(pk=1)
        context.update(
            {
                "plan": queryset.first() if queryset.exists() else None,
                "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLISHABLE_KEY,
            }
        )

        return context
