def identify_router(graph):
    """
    Problem Overview: Network Failure Point.
    Complexity: O(n)
    """
    # An empty dictionary to store the node data
    data = dict()

    # Get all the nodes from the graph in a list of nodes
    nodes = graph.replace(" -> ", ",").split(",")

    # Iterate all the nodes to find the busiest node
    for index, node in enumerate(nodes, start=0):
        # First node
        if index == 0:
            # Increment the first node by +1
            data[node] = 1

        # Last node
        elif index + 1 == len(nodes):
            # Increment the previous node by +1
            data[node] += 1

        # Middle nodes
        else:
            # Increment the node by +2
            if node in data:
                value = data[node]
                value += 2
                data[node] = value
            else:
                data[node] = 2

    # Get the max value for a node
    max_value = max(data.values())
    # Get all the nodes with the max value
    busy_nodes = [key for key, value in data.items() if value == max_value]

    # Output formatting as tring
    max_str = ", "
    max_str = max_str.join(busy_nodes)
    print(max_str)


# Driver function
if __name__ == "__main__":
    # Input the graph of nodes representing the total network
    graph = input()
    identify_router(graph)
