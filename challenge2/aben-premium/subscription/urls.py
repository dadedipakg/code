from django.urls import path

from subscription.views import (
    SubscriptionUpgrade,
    SubscriptionSuccess,
    SubscriptionFailure,
    StripeWebhook,
    SubscriptionCancel,
    SubscriptionCancelSuccess,
)


urlpatterns = [
    path(
        "webhook/",
        StripeWebhook.as_view(),
        name="webhook"
    ),
    path(
        "checkout/<int:pk>/",
        SubscriptionUpgrade.as_view(),
        name="checkout"
    ),
    path(
        "success/",
        SubscriptionSuccess.as_view(),
        name="success"
    ),
    path(
        "failure/",
        SubscriptionFailure.as_view(),
        name="failure"
    ),
    path(
        "cancel-subscription/",
        SubscriptionCancel.as_view(),
        name="cancel-subscription",
    ),
    path(
        "cancelled/",
        SubscriptionCancelSuccess.as_view(),
        name="cancelled"
    ),
]
