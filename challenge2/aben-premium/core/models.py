from django.db import models


class NamedModel(models.Model):
    """
    An abstract base model that provides a mandatory `name` field.
    """

    name = models.CharField(max_length=100)

    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.name}"


class TimeStampedModel(models.Model):
    """
    An abstract base model that provides self-updating
    `created` and `updated` fields.
    """

    created = models.DateTimeField(
        verbose_name="created at",
        auto_now_add=True,
        blank=True,
        db_index=True,
    )
    updated = models.DateTimeField(
        verbose_name="updated at",
        auto_now=True,
        blank=True,
    )

    class Meta:
        abstract = True
